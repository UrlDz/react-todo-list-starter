import DoneItem from "./DoneItem";
import { useSelector } from "react-redux"

export default function DoneGroup() {
    const DoneTodoTasks = useSelector(state => state.todo.tasks).filter(task =>task.done)
    return (
        <div className="todo-group">
            {DoneTodoTasks.map(((todoTask) =>
                <DoneItem key= {todoTask.id} task={todoTask}></DoneItem>
            ))}
        </div>
    );
}