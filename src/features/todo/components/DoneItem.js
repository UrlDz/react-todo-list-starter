
import { useNavigate } from "react-router-dom";
import { useTodo } from "../../hooks/useTodo";


export default function DoneItem(props) {
    const { deleteTodo } = useTodo()
    const navigate = useNavigate();
    const handleTaskNameClick = async () => {
        navigate(`/done/${props.task.id}`)
    }
    const handleRemoveButtonClick = async () => {
        if (window.confirm('Are you sure you wish to delete this item?')) {
            await deleteTodo(props.task.id);
        }
    }
    return (
        <div className='todo-item'>
            <div className={`task-name ${props.task.done ? 'done' : ''}`} onClick={handleTaskNameClick}
            >{props.task.name}
            </div>
            <div className='remove-button' onClick={handleRemoveButtonClick}>x</div>
        </div>
    );
}