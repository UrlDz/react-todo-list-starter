import { useParams } from "react-router-dom";
import { useTodo } from "../../hooks/useTodo";

export default function DoneDetail() {
    const { id } = useParams();
    const { getTodoById } = useTodo()
    const todoTask = async () => {
        await getTodoById(id);
    }
    return (
        <div>
            <h1>DoneDetail</h1>
            <div>{id}</div>
            <div>{todoTask.name}</div>
        </div>
    )
}