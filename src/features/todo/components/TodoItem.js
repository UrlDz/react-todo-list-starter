import { useTodo } from "../../hooks/useTodo";
import { FormOutlined, CloseOutlined, InfoCircleOutlined } from '@ant-design/icons';
export default function TodoItem({ task }) {
    const { updateTodo, deleteTodo, getTodoById } = useTodo()

    const handleTaskNameClick = async () => {
        await updateTodo(task.id, { done: !task.done });
    }

    const hanlleRemoveButtonClick = async () => {
        if (window.confirm('Are you sure you wish to delete this item?')) {
            await deleteTodo(task.id);
        }
    }
    const handleUpdateButtonClick = async () => {
        const updatetext = window.prompt("please enter updatetext");
        if (updatetext) {
            await updateTodo(task.id, { name: updatetext });
        } else {
            window.alert("cancle update")
        }
    }

    const handleDetailButtonClick = async () => {
        await getTodoById(task.id);
        window.confirm("todo id:" + task.id + "\ntodo name:" + task.name);
    }


    return (
        <div className='todo-item'>
            <div className={`task-name ${task.done ? 'done' : ''}`}
                onClick={handleTaskNameClick}>{task.name}
            </div>
            <div className="action">
                <InfoCircleOutlined onClick={handleDetailButtonClick} />
                {!task.done && <FormOutlined onClick={handleUpdateButtonClick} />}
                <CloseOutlined className='remove-button' onClick={hanlleRemoveButtonClick}>x</CloseOutlined>
            </div>
        </div>

    );
}
