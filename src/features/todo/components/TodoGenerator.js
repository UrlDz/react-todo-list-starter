import { useState } from "react";
import { useTodo } from "../../hooks/useTodo";
import { Button } from 'antd';


export default function TodoGenerator() {
    const { addTodo } = useTodo()
    const [taskName, setTaskName] = useState("");

    const handleTaskNameChange = (e) => {
        const value = e.target.value;
        setTaskName(value);
    }

    const handleAddTodoTask = async () => {
        await addTodo({ name: taskName });
        setTaskName("");
    }
    return (
        <div className='todo-generator'>
            <input placeholder='input a new todo here...' onChange={handleTaskNameChange} value={taskName}></input>
            <Button type="primary" onClick={handleAddTodoTask} disabled={!taskName} > Add </Button>
        </div>
    );
}

