import * as todoApi from "../../apis/todo";
import { useDispatch } from "react-redux";
import { initTodoTask } from "../todo/reducers/todoSlice"

export const useTodo = () => {

    const dispatch = useDispatch()
    async function loadTodos() {
        const response = await todoApi.getTodoTasks();
        dispatch(initTodoTask(response.data));
    }
    const updateTodo = async (id, todoTask) => {
        await todoApi.updateTodoTask(id, todoTask);
        await loadTodos();
    }

    const deleteTodo = async (id) => {
        await todoApi.deleteTodoTask(id);
        await loadTodos();
    }
    const addTodo = async (todoTask) => {
        await todoApi.addTodoTask(todoTask);
        await loadTodos();
    }
    const getTodoById = async (id) => {
        await todoApi.getTodoById(id);
        await loadTodos();
    }
    return {
        loadTodos, updateTodo, deleteTodo, addTodo, getTodoById
    }
}