import axios from "axios";
export const api = axios.create({
    baseURL: 'http://localhost:8011/'
    // baseURL: 'https://64c0b6770d8e251fd112652c.mockapi.io/'
})

export default api;