import DoneGroup from "../features/todo/components/DoneGroup";

export default function DoneList() {
    return (
        <div className="done-list">
            <DoneGroup />
        </div>
    )
}