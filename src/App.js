import { NavLink, Outlet } from 'react-router-dom';
import './App.css';

function App() {
  return (
    <div className="app">
      <div className='nav-bar'>
        <nav>
          <ul>
            <li>
              <NavLink to='/'>TodoList</NavLink>
            </li>
            <li>
              <NavLink to='/done'>DoneList</NavLink>
            </li>
            <li>
              <NavLink to='/help'>Help</NavLink>
            </li>
          </ul>
        </nav>
      </div>
      <Outlet />
    </div>
  );
}
export default App;
